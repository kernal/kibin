package main

import (
	"context"
	"flag"
	"fmt"
	"io"
	"log"
	"math/big"
	"net/http"
	"net/url"
	"os"
	"os/signal"
	"path"
	"path/filepath"
	"syscall"
	"time"

	"github.com/go-chi/chi/v5"
	"github.com/go-chi/chi/v5/middleware"
	"github.com/shengdoushi/base58"
	"golang.org/x/net/http2"
	"golang.org/x/net/http2/h2c"
)

const appVersion = "0.1.0"
const appName = "kibin"

type config struct {
	bindAddr string
	baseDir string
	baseUrl string
}

var c config

func readConfig() {
	flag.StringVar(&c.bindAddr, "a", "localhost:8000", "Address to bind (ip:port)")
	flag.StringVar(&c.baseDir, "d", "./files", "File storage directory");
	flag.StringVar(&c.baseUrl, "u", "", "Base URL (defaults to bind)");
	flag.Parse()
	if c.baseUrl == "" {
		c.baseUrl = "http://" + c.bindAddr
	}
}

func generateFilePath(file string) (string, error) {
	var i uint64 = 1;
	var b big.Int
	for {
		b.SetUint64(i)
		d := base58.Encode(b.Bytes(), base58.BitcoinAlphabet)
		p := filepath.Join(c.baseDir, d)
		if _, err := os.Stat(p); os.IsNotExist(err) {
			if err := os.MkdirAll(p, 0755); err != nil {
				return "", err
			}
		}
		f := filepath.Join(d, file)
		if _, err := os.Stat(filepath.Join(c.baseDir, f)); os.IsNotExist(err) {
			return f, nil
		}
		i++
	}
}

func uploadHandler(w http.ResponseWriter, r *http.Request) {
	ff, ffh, err := r.FormFile("file")
	if err != nil {
		log.Println(err)
		return
	}
	defer ff.Close()
	n, err := generateFilePath(ffh.Filename)
	if err != nil {
		log.Println(err)
		return
	}
	df, err := os.Create(filepath.Join(c.baseDir, n))
	if err != nil {
		log.Println(err)
		return
	}
	defer df.Close()
	if _, err := io.Copy(df, ff); err != nil {
		log.Println(err)
		return
	}
	u, _ := url.Parse(c.baseUrl)
	u.Path = path.Join(u.Path, n)
	us := u.String()
	fmt.Fprintln(w, us)
	log.Println("Uploaded file url: '" + us + "'")
	log.Println("Uploaded file size:", ffh.Size, "Bytes")
}

func middlewareServerHeader(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		w.Header().Set("Server", appName + "/" + appVersion)
		next.ServeHTTP(w, r)
	})
}


func initRouter() http.Handler {
	r := chi.NewRouter()
	r.Use(middleware.RequestID)
	r.Use(middleware.RealIP)
	r.Use(middleware.Logger)
	r.Use(middleware.Recoverer)
	r.Use(middlewareServerHeader)
	r.Post("/", uploadHandler)
	r.Mount("/", http.FileServer(http.Dir(c.baseDir)))
	return r
}

func runServer() {
	h2s := &http2.Server{}
	srv := &http.Server{
		Addr: c.bindAddr,
		Handler: h2c.NewHandler(initRouter(), h2s),
	}
	serverCtx, serverStopCtx := context.WithCancel(context.Background())
	sig := make(chan os.Signal, 1)
	signal.Notify(sig, syscall.SIGHUP, syscall.SIGINT, syscall.SIGTERM, syscall.SIGQUIT)
	go func() {
		<-sig
		shutdownCtx, _ := context.WithTimeout(serverCtx, 30 * time.Second)
		go func() {
			<-shutdownCtx.Done()
			if shutdownCtx.Err() == context.DeadlineExceeded {
				log.Fatal("Graceful shutdown timed out... Forcing exit.")
			}
		}()
		err := srv.Shutdown(shutdownCtx)
		if err != nil {
			log.Fatal(err)
		}
		serverStopCtx()
	}()
	err := srv.ListenAndServe()
	if err != nil && err != http.ErrServerClosed {
		log.Fatal(err)
	}
	<-serverCtx.Done()
}

func main() {
	readConfig()
	runServer()
}
