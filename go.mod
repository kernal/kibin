module gitlab.com/kernal/kibin

go 1.16

require (
	github.com/go-chi/chi/v5 v5.0.7
	github.com/shengdoushi/base58 v1.0.0
	golang.org/x/net v0.0.0-20220225172249-27dd8689420f
)
